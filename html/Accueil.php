<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="../frameworkCss/stylesheets/screen.css">
		<link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
		<title>Racoin.net site d'annonce</title>
	</head>
	<body>
		<!-- Le header -->
		<header>
			<?php include("menu.php"); ?>
			<section>
					<div class="ligne">
						<form method="post" action="accueil.html" class="span8 span-m-8 span-l-4 offset-l-2">
							<select id="allWidth">
								<option>Catégorie</option>
								<option value="immobilier">Immobilier</option>
								<option value="vehicule">Véhicule</option>
								<option value="jeux">Jeux/Jouet</option>
								<option value="vetement">Vetement</option>
								<option value="telephonie">Téléphonie</option>
								<option value="livre">Livres</option>
							</select>
						</form>
					</div>
			</section>
		</header>
		<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
		<section>
			<a href="DetailAnnonce.php">
				<article class="ligne">
					<div class="span1 span-l-1">Aujourd'hui 12:15</div>
					<img src="../images/voiture.jpg" alt="" class="span2 span-l-2">
					<div class="span5 span-l-4">
						<h2>Abarth simca 2000 gt-1963</h2>
						<div>Prix: 30€<br />catégorie: Jeux/Jouet</div>
						<p class="hidden">Description de l'annonce Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
						Mollitia illo delectus, vitae esse, maiores consequuntur quis voluptate autem! Officiis, atque.</p>
						<div>Meurthe-et-Moselle</div>
					</div>
				</article>
			</a>
			<hr>
			<article class="ligne">
				<div class="span1">Aujourd'hui 11:20</div>
				<img src="../images/vetement.jpg" alt="" class="span2">
				<div class="span5">
					<h2>Veste cuir IKKS écrue T.4 ans</h2>
					<div>Prix: 40€<br />catégorie: Vetement</div>
					<p class="hidden">Description de l'annonce Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
					Mollitia illo delectus, vitae esse, maiores consequuntur quis voluptate autem! Officiis, atque.</p>
					<div>Meurthe-et-Moselle</div>
				</div>
			</article>
			<hr>
			<article class="ligne">
				<div class="span1">Aujourd'hui 10:23</div>
				<img src="../images/telephone.jpg" alt="" class="span2">
				<div class="span5">
					<h2>Galaxy S3 4G blanc 16Go</h2>
					<div>Prix: 150€<br />catégorie: Téléphonie</div>
					<p class="hidden">Description de l'annonce Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
					Mollitia illo delectus, vitae esse, maiores consequuntur quis voluptate autem! Officiis, atque.</p>
					<div>Meurthe-et-Moselle</div>
				</div>
			</article>
			<hr>
			<article class="ligne">
				<div class="span1">Aujourd'hui 09:23</div>
				<img src="../images/maison.jpg" alt="" class="span2">
				<div class="span5">
					<h2>Maison de plein pied</h2>
					<div>Prix: 150 000€<br />catégorie: Immobilier</div>
					<p class="hidden">Description de l'annonce Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
					Mollitia illo delectus, vitae esse, maiores consequuntur quis voluptate autem! Officiis, atque.</p>
					<div>Meurthe-et-Moselle</div>
				</div>
			</article>
			<hr>
			<article class="ligne">
				<div class="span1">Aujourd'hui 08:15</div>
				<img src="../images/livres.jpg" alt="" class="span2">
				<div class="span5">
					<h2>Collection de livres</h2>
					<div>Prix: 30€<br />catégorie: Livres</div>
					<p class="hidden">Description de l'annonce Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
					Mollitia illo delectus, vitae esse, maiores consequuntur quis voluptate autem! Officiis, atque.</p>
					<div>Meurthe-et-Moselle</div>
				</div>
			</article>
		</section>
		<?php include("footer.php"); ?>
	</body>
</html>