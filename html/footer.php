<footer class="ligne">
	<section class="span3">
		<h1>Racoin.net</h1>
		<address>
			2 ter Boulevard Charlemagne<br />
			54 000 Nancy<br />
			+33 3 54 50 38 00
		</address>
	</section>
	<section class="span5">
		<h1>Suivez-nous :</h1>
		<a href="twitter.ocm"><img src="../images/twitter.png" alt="nous suivre sur twitter"></a>
		<a href="google.com"><img src="../images/google_plus.png" alt="nous suivre sur googleplus"></a>
		<a href="facebook.com"><img src="../images/facebook.png" alt="nous suivre sur facebook"></a>
	</section>
</footer>