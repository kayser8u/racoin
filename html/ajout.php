<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="../frameworkCss/stylesheets/screen.css">
		<link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
		<title>Racoin.net site d'annonce</title>
	</head>
	<body>
		<header>
			<?php include("menu.php"); ?>
		</header>
		<br /><br /><br /><br />
		<section>
			<article>
				<fieldset class="ligne">
					<legend>Ajouter une annonce</legend>
				<div class="ligne">
					<label for="l_titre_annonce" class="span2 offset1">Titre de l'annonce :</label>
					<input type="text" name="it_titre_annonce" class="span4"/>
			
			
					<label for="l_description" class="span2 offset1">Description :</label>
			
					<textarea name="ta_description" rows="4" class="span6 offset1"></textarea><br />
			
					<br><label for="l_prix" class="offset1 span2 span-l-1">Prix :</label>
					<input type="text" name="it_prix" class="span2 span-l-2"/>
				
					<label for="l_email" class="offset1 span2 span-l-1 offset-l-0">Email :</label>
					<input type="text" name="it_email" class="span3 span-l-2">
				
					<label for="l_nom" class="offset1 span2 span-l-1 ">Nom :</label>
					<input type="text" name="it_nom" class="span3 span-l-2"/>
				
					<label for="l_password" class="offset1 span2 span-l-1 offset-l-0">Mot de passe:</label>
					<input type="password" name="it_password" class="span3 span-l-2"/><br>
				
					<label for="l_region" class="offset1 span2 offset-l-2 span-l-1">Region:</label>
					<select name="s_region" class="span3 span-l-3">
						<option>Lorraine</option>
						<option>Alsace</option>
						<option>Picardie</option>
						<option>Paca</option>
					</select><br>
					<label for="l_image" class="span2 offset1 offset-l-2 span-l-1">Images :</label>
					<button type="submit" name="b_parcourir" value="Parcourir" class="span3">Parcourir</button><br>
					<!-- <input type="file" name="b_parcourir" value="Parcourir" class="span3">Parcourir/><br> -->
				</div>
				</fieldset>
				<br>
				<div class="ligne">
					<img src="../images/voiture.jpg" class="span2">
					<img src="../images/voiture.jpg" class="span2 offset1">
					<img src="../images/voiture.jpg" class="span2 offset1">
				</div><br>
				<div class="ligne">
					<button type="submit" name="b_valider" value="Valider" class="span2 offset1 offset-l-2 span-l-2">Valider</button>
					<button type="submit" name="b_annuler" value="Annuler" class="span2 offset2 offset-l-0 span-l-2">Annuler</button><br>
				</div><br>
			</article>
		</section>
		<?php include("footer.php"); ?>
	</body>
</html>