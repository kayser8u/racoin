<!DOCTYPE>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="../frameworkCss/stylesheets/screen.css">
		<link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
		<title>Racoin.net site d'annonce</title>
	</head>
	<body>
		<!-- Le header -->
		<header>
			<?php include("menu.php"); ?>
		</header>
		<br /><br /><br />
		<!-- Le formulaire de recherche -->
		<fieldset class="ligne">
			<legend>Recherche</legend>
			<form action="index.php" method="post" class="ligne">
				<input type="text" class="span8 span-l-4 offset-l-2">
				<div class="ligne">
					<select name="categorie" id="categorie" class="span8 span-l-2 offset-l-2">
						<option value="Voiture">Voiture</option>
						<option value="Informatique">Informatique</option>
						<option value="Telephonie">Telephonie</option>
						<option value="Jardinage">Jardinage</option>
					</select>

					<select name="departement" id="departement" class="span8 span-l-2">
						<option value="Alsace">Alsace</option>
						<option value="Alsace">Bourgogne</option>
						<option value="Alsace">Picardie</option>
						<option value="Alsace">Lorraine</option>
					</select>
				</div>
<!-- 				<select name="prix" id="prix" class="span1"> -->
<!-- 					<option value="0-50">0 : 50 &euro;</option> code HTML de € est &euro;  -->
<!-- 					<option value="50-150">50 :150 &euro;</option> -->
<!-- 					<option value="150-300">150 : 300 &euro;</option> -->
<!-- 					<option value="300">Plus de 300 &euro;</option> -->
<!-- 				</select> -->

				<button type="submit" name="rechercher" value="Rechercher" class="span8 span-l-4 offset-l-2">Rechercher</button>
			</form>
		</fieldset>

		<!-- Emplacement des annonces trouvées par la recherche -->
		<section>
			<a href="DetailAnnonce.php">
				<article class="ligne">
					<div class="span1">Aujourd'hui 12:15</div>
					<img src="../images/voiture.jpg" alt="" class="span2">
					<div class="span5">
						<h2>Abarth simca 2000 gt-1963</h2>
						<div>Prix: 30€<br />catégorie: Jeux/Jouet</div>
						<p class="hidden">Description de l'annonce Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
						Mollitia illo delectus, vitae esse, maiores consequuntur quis voluptate autem! Officiis, atque.</p>
						<div>Meurthe-et-Moselle</div>
					</div>
				</article>
			</a>
			<hr>
			<article class="ligne">
				<div class="span1">Aujourd'hui 11:20</div>
				<img src="../images/vetement.jpg" alt="" class="span2">
				<div class="span5">
					<h2>Veste cuir IKKS écrue T.4 ans</h2>
					<div>Prix: 40€<br />catégorie: Vetement</div>
					<p class="hidden">Description de l'annonce Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
					Mollitia illo delectus, vitae esse, maiores consequuntur quis voluptate autem! Officiis, atque.</p>
					<div>Meurthe-et-Moselle</div>
				</div>
			</article>
			<hr>
			<article class="ligne">
				<div class="span1">Aujourd'hui 10:23</div>
				<img src="../images/telephone.jpg" alt="" class="span2">
				<div class="span5">
					<h2>Galaxy S3 4G blanc 16Go</h2>
					<div>Prix: 150€<br />catégorie: Téléphonie</div>
					<p class="hidden">Description de l'annonce Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
					Mollitia illo delectus, vitae esse, maiores consequuntur quis voluptate autem! Officiis, atque.</p>
					<div>Meurthe-et-Moselle</div>
				</div>
			</article>
		</section>
		<?php include("footer.php"); ?>
	</body>
</html>