<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="../frameworkCss/stylesheets/screen.css">
		<link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
		<title>Racoin.net site d'annonce</title>
	</head>
	<body>
		<header>
			<?php include("menu.php"); ?>
		</header>
		<br /><br /><br /><br /><br /><br /><br />
		<nav>
			<a href="#" class="btn">Retour aux résultats de recherche</a></li>
		</nav>
		<section>
			<article>
				<h1>Abarth simca 2000 gt-1963</h1>
				<img src="../images/voiture.jpg" alt="image principale">
				<p>Description de l'annonce Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
				Mollitia illo delectus, vitae esse, maiores consequuntur quis voluptate autem! Officiis, atque.</p>
				<a href="mailto:">Contacter le vendeur</a>
				<div class="ligne">
					<img src="../images/voiture.jpg" alt="" class="span2 offset1">
					<img src="../images/voiture.jpg" alt="" class="span2">
					<img src="../images/voiture.jpg" alt="" class="span2">
				</div>
			</article>
			<form action="index.html" method="post">
				<button type="button" name="modify" value="modifier">Modifier</button>
				<button type="button" name="delete" value="supprimer">Supprimer</button>
			</form>
		</section>
		<?php include("footer.php"); ?>
	</body>
</html>